package com.krzysztofcywinski.fara.presentation.map

import com.google.android.gms.maps.model.Marker
import com.krzysztofcywinski.fara.domain.model.ClosestDepartureStop
import com.krzysztofcywinski.fara.domain.model.LineStops
import com.krzysztofcywinski.fara.domain.model.Stop
import com.krzysztofcywinski.fara.domain.model.map.MapModel
import com.krzysztofcywinski.fara.navigation.Navigator
import io.reactivex.Flowable
import org.joda.time.LocalTime
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class MapPresenterTest {

    private lateinit var presenter: MapPresenter

    @Mock
    private lateinit var mockedView: MapActivity

    @Mock
    private lateinit var mockedMapModel: MapModel

    @Mock
    private lateinit var mockedNavigator: Navigator

    @Mock
    private lateinit var marker: Marker

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = MapPresenter(mockedView, mockedMapModel, mockedNavigator)
    }

    @Test
    fun `Should start listening for location after location permissions is granted`() {
        // when
        presenter.onLocationPermissionsGranted()

        // then
        verify(mockedMapModel).startListeningForLocation()
    }

    @Test
    fun `Should show stops and closest stop`() {
        // given
        val lineStops = LineStops()
        val stopList: MutableList<Stop> = mutableListOf<Stop>()
        lineStops.stops = stopList
        val stop = Stop()
        stop.direction = "Banacha"
        stop.name = "Centrum 07"
        stop.stopNr = "07"
        stop.stopId = 1234L
        stop.lon = 12.22
        stop.lat = 22.22
        stop.line = "35"
        stopList.add(stop)

        val closestStop = ClosestDepartureStop()
        closestStop.direction = "Banacha"
        closestStop.name = "Centrum 07"
        closestStop.stopNr = "07"
        closestStop.stopId = 1234L
        closestStop.lon = 12.22
        closestStop.lat = 22.22
        closestStop.line = "35"
        closestStop.departureTime = LocalTime.now()

        Mockito.`when`(mockedMapModel.onLineStops).thenReturn(Flowable.just(lineStops))
        Mockito.`when`(mockedMapModel.closestStop).thenReturn(Flowable.just(closestStop))

        // when
        presenter.onMapReady()

        // then
        verify(mockedView).printStop(stop)
        verify(mockedView).setAllStopsDefaultColor()
        verify(mockedView).showClosestStop(closestStop)
    }

    @Test
    fun `Should go to Time table with selected stop`() {
        // given
        val lineStops = LineStops()
        val stopList: MutableList<Stop> = mutableListOf<Stop>()
        lineStops.stops = stopList
        val stop = Stop()
        stop.direction = "Banacha"
        stop.name = "Centrum 07"
        stop.stopNr = "07"
        stop.stopId = 1234L
        stop.lon = 12.22
        stop.lat = 22.22
        stop.line = "35"
        stopList.add(stop)

        val closestStop = ClosestDepartureStop()
        closestStop.direction = "Banacha"
        closestStop.name = "Centrum 07"
        closestStop.stopNr = "07"
        closestStop.stopId = 1234L
        closestStop.lon = 12.22
        closestStop.lat = 22.22
        closestStop.line = "35"
        closestStop.departureTime = LocalTime.now()

        Mockito.`when`(mockedMapModel.onLineStops).thenReturn(Flowable.just(lineStops))
        Mockito.`when`(mockedMapModel.closestStop).thenReturn(Flowable.just(closestStop))
        Mockito.`when`(marker.title).thenReturn(stop.name)

        // when
        presenter.onMapReady()
        presenter.onMarkerTextClicked(marker)

        // then
        verify(mockedNavigator).goToTimeTable(stop)
    }

}