package com.krzysztofcywinski.fara.repository

import android.annotation.SuppressLint
import android.app.Activity
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import com.krzysztofcywinski.fara.domain.model.LineStops
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.processors.BehaviorProcessor
import io.reactivex.subjects.BehaviorSubject

class LocationRepository(private val activity: Activity) {

    private val LOCATION_REFRESH_TIME: Long = 10000
    private val LOCATION_REFRESH_DISTANCE: Float = 100f

    private val locationProcessor = BehaviorSubject.create<Location>()

    val onLocationChanged: Flowable<Location> = locationProcessor.toFlowable(BackpressureStrategy.LATEST)

    private val locationListener = object : LocationListener {
        override fun onProviderEnabled(p0: String?) {}

        override fun onProviderDisabled(p0: String?) {}

        override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {}

        override fun onLocationChanged(location: Location) {
            locationProcessor.onNext(location)
        }
    }

    @SuppressLint("MissingPermission")
    fun startListeningForLocation() {
        val locationManager = activity.getSystemService(FragmentActivity.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_REFRESH_TIME,
                LOCATION_REFRESH_DISTANCE, locationListener)
        val lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
        if (lastKnownLocation != null) {
            locationProcessor.onNext(lastKnownLocation)
        }
    }
}