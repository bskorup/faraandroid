package com.krzysztofcywinski.fara.domain.model.map

import com.krzysztofcywinski.fara.domain.model.ClosestDepartureStop
import com.krzysztofcywinski.fara.domain.model.LineStops
import com.krzysztofcywinski.fara.domain.model.Stop
import com.krzysztofcywinski.fara.domain.repository.LineStopsRepository
import com.krzysztofcywinski.fara.domain.repository.ZtmRepository
import com.krzysztofcywinski.fara.repository.LocationRepository
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.joda.time.LocalTime

class MapModel(private val lineStopsRepository: LineStopsRepository,
               private val locationRepository: LocationRepository,
               private val ztmRepository: ZtmRepository) {

    val onLineStops: Flowable<LineStops> = lineStopsRepository.onLineStops

    val closestStop: Flowable<ClosestDepartureStop> = onLineStops.flatMap {
        val stops = it.stops
        locationRepository.onLocationChanged.flatMap {
            val closestDepartureStop = ClosestDepartureStop()
            var closestDistance: Double = Double.MAX_VALUE
            for (stop: Stop in stops!!) {
                val currentDistance = distance(stop.lat!!, it.latitude,
                        stop.lon!!, it.longitude, 0.0, 0.0)
                if (currentDistance < closestDistance) {
                    closestDistance = currentDistance
                    closestDepartureStop.direction = stop.direction
                    closestDepartureStop.name = stop.name
                    closestDepartureStop.lat = stop.lat
                    closestDepartureStop.line = stop.line
                    closestDepartureStop.lon = stop.lon
                    closestDepartureStop.stopId = stop.stopId
                    closestDepartureStop.stopNr = stop.stopNr
                }
            }
            ztmRepository.getTimeTable(closestDepartureStop.stopId!!,
                    closestDepartureStop.stopNr!!, closestDepartureStop.line!!)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .toFlowable(BackpressureStrategy.LATEST)
                    .map {
                        val now = LocalTime.now()
                        val sortedElements = it.elements!!.sortedWith(compareBy({ it.time }))
                        for (element in sortedElements) {
                            if (element.time!!.isAfter(now)) {
                                //TODO: refresh nearest departure when time elapsed
                                closestDepartureStop.departureTime = element.time!!
                                break
                            }
                        }
                        closestDepartureStop
                    }
        }
    }

    fun startListeningForLocation() {
        locationRepository.startListeningForLocation()
    }

    /**
     * Calculate distance between two points in latitude and longitude taking
     * into account height difference. If you are not interested in height
     * difference pass 0.0. Uses Haversine method as its base.
     *
     * lat1, lon1 Start point lat2, lon2 End point el1 Start altitude in meters
     * el2 End altitude in meters
     * @returns Distance in Meters
     */
    fun distance(lat1: Double, lat2: Double, lon1: Double,
                 lon2: Double, el1: Double, el2: Double): Double {

        val R = 6371 // Radius of the earth

        val latDistance = Math.toRadians(lat2 - lat1)
        val lonDistance = Math.toRadians(lon2 - lon1)
        val a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) +
                (Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                        * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2))
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        var distance = R.toDouble() * c * 1000.0 // convert to meters

        val height = el1 - el2

        distance = Math.pow(distance, 2.0) + Math.pow(height, 2.0)

        return Math.sqrt(distance)
    }
}
