package com.krzysztofcywinski.fara.presentation.timetable

import android.support.annotation.LayoutRes
import android.support.v7.widget.LinearLayoutManager
import com.krzysztofcywinski.fara.R
import com.krzysztofcywinski.fara.domain.model.TimeTableHourElement
import com.krzysztofcywinski.fara.mvp.BaseActivityView
import kotlinx.android.synthetic.main.activity_time_table.*
import javax.inject.Inject


class TimeTableActivity : BaseActivityView() {

    @LayoutRes
    override val layoutResId = R.layout.activity_time_table

    @Inject
    lateinit var injectedPresenter: TimeTablePresenter

    override val presenter: TimeTablePresenter get() = injectedPresenter

    override fun onViewBound() {
    }

    fun showTimeTable(elements: List<TimeTableHourElement>) {
        timeTable.layoutManager = LinearLayoutManager(this)
        val adapter = TimeTableAdapter(elements, this)
        timeTable.setAdapter(adapter)
    }

    fun setHeader(line: String, name: String, direction: String) {
        header.text = String.format(getString(R.string.time_table_header), line, name, direction)
    }

    override fun onBackPressed() {
        presenter.onBackPressed()
    }
}
