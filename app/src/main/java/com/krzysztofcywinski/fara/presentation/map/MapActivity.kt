package com.krzysztofcywinski.fara.presentation.map

import android.Manifest
import android.annotation.SuppressLint
import android.content.IntentSender
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.krzysztofcywinski.fara.R
import com.krzysztofcywinski.fara.domain.model.ClosestDepartureStop
import com.krzysztofcywinski.fara.domain.model.Stop
import com.krzysztofcywinski.fara.mvp.BaseActivityView
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.activity_map.*
import org.joda.time.format.DateTimeFormat
import javax.inject.Inject

class MapActivity : BaseActivityView(), OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private val REQUEST_CHECK_SETTINGS: Int = 123

    private val WARSAW_LAT = 52.217049
    private val WARSAW_LNG = 21.017532

    @LayoutRes
    override val layoutResId = R.layout.activity_map

    @Inject
    lateinit var injectedPresenter: MapPresenter

    private var map: GoogleMap? = null
    private var googleApiClient: GoogleApiClient? = null
    private val markers = HashMap<String, Marker>()

    override val presenter: MapPresenter get() = injectedPresenter

    override fun onViewBound() {
        val rxPermissions = RxPermissions(this)
        if (!rxPermissions.isGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
            rxPermissions
                    .request(Manifest.permission.ACCESS_FINE_LOCATION)
                    .subscribe({ granted ->
                        if (granted!!) { // Always true pre-M
                            displayLocationSettingsRequest()
                        }
                    })
        } else {
            displayLocationSettingsRequest()
        }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map!!.isMyLocationEnabled = true

        val center = CameraUpdateFactory.newLatLng(LatLng(WARSAW_LAT, WARSAW_LNG))
        val zoom = CameraUpdateFactory.zoomTo(12f)

        map!!.moveCamera(center)
        map!!.animateCamera(zoom)

        map!!.setOnInfoWindowClickListener { marker ->
            presenter.onMarkerTextClicked(marker)
        }

        presenter.onMapReady()
    }

    fun printStop(stop: Stop) {
        val circleDrawable = resources.getDrawable(R.drawable.ic_stop, theme)
        val icon = getMarkerIconFromDrawable(circleDrawable)

        val position = LatLng(stop.lat!!, stop.lon!!)
        markers.put(stop.name!!, map!!.addMarker(MarkerOptions().icon(icon).position(position).title(stop.name)))
    }

    private fun getMarkerIconFromDrawable(drawable: Drawable): BitmapDescriptor {
        val canvas = Canvas()
        val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        canvas.setBitmap(bitmap)
        drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        drawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    private fun displayLocationSettingsRequest() {
        presenter.onLocationPermissionsGranted()
        if (googleApiClient == null) {
            googleApiClient = GoogleApiClient.Builder(this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build()
            googleApiClient!!.connect()

            val locationRequest = LocationRequest.create()
            locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            locationRequest.interval = 30 * 1000
            locationRequest.fastestInterval = 5 * 1000
            val builder = LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest)

            builder.setAlwaysShow(true)

            val task = LocationServices.getSettingsClient(this).checkLocationSettings(builder.build())

            task.addOnCompleteListener {
                try {
                    val response = it.getResult(ApiException::class.java)
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                } catch (exception: ApiException) {
                    when (exception.statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                            try {
                                val resolvable = exception as ResolvableApiException
                                resolvable.startResolutionForResult(
                                        this@MapActivity,
                                        REQUEST_CHECK_SETTINGS)
                            } catch (e: IntentSender.SendIntentException) {
                                // Ignore the error.
                            } catch (e: ClassCastException) {
                                // Ignore, should be an impossible error.
                            }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE ->
                            Toast.makeText(this@MapActivity,
                                    getString(R.string.location_settings_change_error),
                                    Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Toast.makeText(this@MapActivity,
                String.format(getString(R.string.connection_failed), p0.errorMessage),
                Toast.LENGTH_LONG).show()
    }

    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
    }

    override fun onConnectionSuspended(p0: Int) {
        Toast.makeText(this@MapActivity,
                getString(R.string.connection_suspended), Toast.LENGTH_LONG).show()
    }

    fun showClosestStop(stop: ClosestDepartureStop) {
        val dtfOut = DateTimeFormat.forPattern("HH:mm")
        closestStop.text = String.format(getString(R.string.closest_stop_info), stop.name, stop.direction, dtfOut.print(stop.departureTime))

        val circleDrawable = resources.getDrawable(R.drawable.ic_stop_closest, theme)
        val icon = getMarkerIconFromDrawable(circleDrawable)

        val marker = markers.get(stop.name)
        marker?.setIcon(icon)
    }

    fun setAllStopsDefaultColor() {
        val circleDrawable = resources.getDrawable(R.drawable.ic_stop, theme)
        val icon = getMarkerIconFromDrawable(circleDrawable)
        for (marker in markers) {
            marker.value.setIcon(icon)
        }
    }
}
