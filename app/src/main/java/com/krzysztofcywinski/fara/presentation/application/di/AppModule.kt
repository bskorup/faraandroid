package com.krzysztofcywinski.fara.presentation.application.di

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import com.krzysztofcywinski.fara.domain.repository.LineStopsRepository
import com.krzysztofcywinski.fara.domain.repository.ZtmRepository
import com.krzysztofcywinski.fara.domain.source.FaraDataSourceImpl
import com.krzysztofcywinski.fara.domain.source.ZtmDataSourceImpl
import com.wdh.remotecontrol.presentation.base.schedulers.AndroidSchedulersProvider
import com.wdh.remotecontrol.presentation.base.schedulers.SchedulersProvider
import dagger.Binds
import dagger.Module
import dagger.Provides
import java.io.File

@Module
abstract class AppModule {

    @Binds
    abstract fun bindsContext(app: Application): Context

    @Module
    companion object {

        @JvmStatic
        private fun provideZtmDataSource(app: Application
        ): ZtmDataSourceImpl = ZtmDataSourceImpl(providesConnectivityManager(app), providesCacheDir(app))

        @JvmStatic
        private fun provideFaraDataSource(app: Application
        ): FaraDataSourceImpl = FaraDataSourceImpl(providesConnectivityManager(app), providesCacheDir(app))

        @JvmStatic
        @Provides
        @AppScope
        fun providesLineStopsRepository(app: Application
        ): LineStopsRepository = LineStopsRepository(provideFaraDataSource(app), providesSchedulersProvider())

        @JvmStatic
        @Provides
        @AppScope
        fun providesZtmRepository(app: Application
        ): ZtmRepository = ZtmRepository(provideZtmDataSource(app))

        @JvmStatic
        fun providesSchedulersProvider(): SchedulersProvider = AndroidSchedulersProvider()

        @JvmStatic
        private fun providesConnectivityManager(app: Application
        ): ConnectivityManager = app.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        @JvmStatic
        private fun providesCacheDir(app: Application
        ): File = app.cacheDir

    }
}