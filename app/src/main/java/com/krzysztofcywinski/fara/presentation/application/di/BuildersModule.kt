package com.krzysztofcywinski.fara.presentation.application.di

import com.krzysztofcywinski.fara.presentation.map.MapActivity
import com.krzysztofcywinski.fara.presentation.map.di.MapActivityModule
import com.krzysztofcywinski.fara.presentation.timetable.TimeTableActivity
import com.krzysztofcywinski.fara.presentation.timetable.di.TimeTableActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuildersModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [TimeTableActivityModule::class])
    abstract fun contributesTimeTableActivity(): TimeTableActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [MapActivityModule::class])
    abstract fun contributesMapActivity(): MapActivity

}