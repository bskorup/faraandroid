package com.krzysztofcywinski.fara.navigation

import android.content.Intent
import com.krzysztofcywinski.fara.domain.model.Stop
import com.krzysztofcywinski.fara.mvp.BaseActivityView
import com.krzysztofcywinski.fara.presentation.map.MapActivity
import com.krzysztofcywinski.fara.presentation.timetable.TimeTableActivity
import javax.inject.Inject

class Navigator @Inject constructor(private val activity: BaseActivityView) {

    fun goToTimeTable(stop: Stop) {
        val intent = Intent(activity, TimeTableActivity::class.java)
        intent.putExtra("Stop", stop)
        activity.startActivity(intent)
        activity.finish()
    }

    fun goToMap() {
        val intent = Intent(activity, MapActivity::class.java)
        activity.startActivity(intent)
        activity.finish()
    }
}