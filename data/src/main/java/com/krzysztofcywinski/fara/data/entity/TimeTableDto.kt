package com.krzysztofcywinski.fara.data.entity

import com.squareup.moshi.Json

class TimeTableDto {

    @Json(name = "result")
    var result: List<TimeTableElementDto>? = null

}
