package com.krzysztofcywinski.fara.data.network.service

import com.krzysztofcywinski.fara.data.entity.TimeTableDto
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ZtmService {

    @GET("api/action/dbtimetable_get/")
    fun getTimeTable(@Query("id") id: String,
                     @Query("busstopId") busstopId: Long,
                     @Query("busstopNr") busstopNr: String,
                     @Query("line") line: String,
                     @Query("apikey") apikey: String
    ): Observable<TimeTableDto>
}
