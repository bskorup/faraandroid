package com.krzysztofcywinski.fara.data.entity

import com.squareup.moshi.Json

class StopDto {

    @Json(name = "direction")
    var direction: String? = null

    @Json(name = "name")
    var name: String? = null

    @Json(name = "lat")
    var lat: Double? = null

    @Json(name = "lon")
    var lon: Double? = null

    @Json(name = "line")
    var line: String? = null

    @Json(name = "stopId")
    var stopId: String? = null

}
