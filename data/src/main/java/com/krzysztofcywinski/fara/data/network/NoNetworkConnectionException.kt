package com.krzysztofcywinski.fara.data.network

class NoNetworkConnectionException : RuntimeException()
