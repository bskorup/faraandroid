package com.krzysztofcywinski.fara.data.network.interceptor

import android.annotation.SuppressLint
import android.net.ConnectivityManager
import com.krzysztofcywinski.fara.data.network.NoNetworkConnectionException
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException

class NetworkConnectionInterceptor(private val connectivityManager: ConnectivityManager) : Interceptor {

    private val isNetworkConnected: Boolean
        @SuppressLint("MissingPermission")
        get() {
            val networkInfo = connectivityManager.activeNetworkInfo
            return networkInfo?.isConnected ?: false
        }

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!isNetworkConnected) {
            throw NoNetworkConnectionException()
        }
        val builder = chain.request().newBuilder()

        return chain.proceed(builder.build())
    }
}
