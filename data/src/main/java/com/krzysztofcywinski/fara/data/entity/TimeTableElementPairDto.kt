package com.krzysztofcywinski.fara.data.entity

import com.squareup.moshi.Json

class TimeTableElementPairDto {

    @Json(name = "key")
    var key: String? = null

    @Json(name = "value")
    var value: String? = null

}
