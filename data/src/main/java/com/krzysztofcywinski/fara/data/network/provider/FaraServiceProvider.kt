package com.krzysztofcywinski.fara.data.network.provider

import android.net.ConnectivityManager
import android.util.Log

import com.krzysztofcywinski.fara.data.network.interceptor.NetworkConnectionInterceptor
import com.krzysztofcywinski.fara.data.network.service.FaraService
import com.squareup.moshi.Moshi

import java.io.File

import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

class FaraServiceProvider
(connectivityManager: ConnectivityManager,
 cacheDir: File, restApiAnonymousBaseUrl: String) {

    private val loggingInterceptor: HttpLoggingInterceptor
    private val networkConnectionInterceptor: NetworkConnectionInterceptor
    private val okHttpClient: OkHttpClient
    private val jsonConverter: Moshi
    private val retrofit: Retrofit

    init {

        loggingInterceptor = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { message -> Log.d("TAG", message) })
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        networkConnectionInterceptor = NetworkConnectionInterceptor(connectivityManager)

        okHttpClient = OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .addInterceptor(networkConnectionInterceptor)
                .cache(Cache(cacheDir, DISK_CACHE_SIZE.toLong()))
                .build()


        jsonConverter = Moshi.Builder().build()

        retrofit = Retrofit.Builder()
                .baseUrl(restApiAnonymousBaseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create(jsonConverter))
                .client(okHttpClient)
                .build()
    }

    fun get(): FaraService {
        return retrofit.create(FaraService::class.java)
    }

    companion object {
        private val DISK_CACHE_SIZE = 10 * 1024 * 1024

        private var instance: FaraServiceProvider? = null

        fun newInstance(connectivityManager: ConnectivityManager,
                        cacheDir: File, restApiAnonymousBaseUrl: String): FaraServiceProvider {
            if (instance == null) {
                instance = FaraServiceProvider(connectivityManager, cacheDir, restApiAnonymousBaseUrl)
            }
            return instance as FaraServiceProvider
        }
    }
}
