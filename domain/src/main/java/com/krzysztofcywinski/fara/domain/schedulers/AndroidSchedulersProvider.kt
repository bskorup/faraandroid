package com.wdh.remotecontrol.presentation.base.schedulers

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AndroidSchedulersProvider : SchedulersProvider {
    override fun background() = Schedulers.io()
    override fun ui() = AndroidSchedulers.mainThread()
}