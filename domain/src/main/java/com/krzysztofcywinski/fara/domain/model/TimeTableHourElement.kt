package com.krzysztofcywinski.fara.domain.model

class TimeTableHourElement {

    var hour: Int = 0

    var minutes: MutableList<Int> = mutableListOf<Int>()

}
