package com.krzysztofcywinski.fara.domain.model

import org.joda.time.LocalTime

class TimeTableElement {

    var time: LocalTime? = null

    var direction: String = ""

}
