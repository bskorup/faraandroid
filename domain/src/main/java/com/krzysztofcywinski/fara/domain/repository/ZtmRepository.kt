package com.krzysztofcywinski.fara.domain.repository

import com.krzysztofcywinski.fara.domain.model.TimeTable
import com.krzysztofcywinski.fara.domain.model.TimeTableHourElement
import com.krzysztofcywinski.fara.domain.source.ZtmDataSourceImpl
import io.reactivex.Observable

class ZtmRepository(private val ztmDataSourceImpl: ZtmDataSourceImpl) {

    private val ID = "e923fa0e-d96c-43f9-ae6e-60518c9f3238"
    private val API_KEY = "171164bd-04c9-4f4c-bc63-9ca055bd6262"

    private val cachedData = HashMap<String, TimeTable>()

    fun getTimeTable(busstopId: Long, busstopNr: String, line: String): Observable<TimeTable> {
        val timeTable = cachedData[getRequestHash(busstopId, busstopNr, line)]
        if (timeTable != null) {
            return Observable.just(timeTable)
        }
        return ztmDataSourceImpl.getTimeTable(ID, busstopId,
                busstopNr, line, API_KEY)
                .map {
                    cachedData.put(getRequestHash(busstopId, busstopNr, line), it)
                    it
                }
    }

    fun getTimeTableHourElements(busstopId: Long, busstopNr: String, line: String): Observable<List<TimeTableHourElement>> {
        return getTimeTable(busstopId, busstopNr, line).map<List<TimeTableHourElement>> {
            parseTimeTable(it)
        }
    }

    private fun parseTimeTable(it: TimeTable?): List<TimeTableHourElement> {
        val list = ArrayList<TimeTableHourElement>()
        for (element in it?.elements!!) {
            if (!list.isEmpty() && list.last().hour == element.time?.hourOfDay) {
                element.time?.minuteOfHour?.let { it1 -> list.last().minutes.add(it1) }
            } else {
                val timeTableHourElement = TimeTableHourElement()
                timeTableHourElement.hour = element.time?.hourOfDay!!
                timeTableHourElement.minutes.add(element.time?.minuteOfHour!!)
                list.add(timeTableHourElement)
            }
        }
        return list
    }

    private fun getRequestHash(busstopId: Long, busstopNr: String, line: String) =
            "" + busstopId + "" + busstopNr + line
}