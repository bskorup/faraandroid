package com.krzysztofcywinski.fara.domain.source

import android.net.ConnectivityManager
import com.krzysztofcywinski.fara.data.network.provider.ZtmServiceProvider
import com.krzysztofcywinski.fara.data.network.service.ZtmService
import com.krzysztofcywinski.fara.domain.datasource.ZtmDataSource
import com.krzysztofcywinski.fara.domain.model.TimeTable
import io.reactivex.Observable
import java.io.File

class ZtmDataSourceImpl(connectivityManager: ConnectivityManager,
                        cacheDir: File) : ZtmDataSource {

    override fun getTimeTable(id: String, busstopId: Long, busstopNr: String, line: String, apikey: String): Observable<TimeTable> {
        return service.getTimeTable(id, busstopId, busstopNr, line, apikey).map {
            TimeTable.fromDto(it.result!!)
        }
    }

    private val service: ZtmService

    init {
        service = ZtmServiceProvider.newInstance(
                connectivityManager,
                cacheDir,
                API_BASE_URL)
                .get()
    }

    companion object {
        var API_BASE_URL = "https://api.um.warszawa.pl/"
    }
}
