package com.krzysztofcywinski.fara.domain.repository

import com.krzysztofcywinski.fara.domain.model.LineStops
import com.krzysztofcywinski.fara.domain.source.FaraDataSourceImpl
import com.wdh.remotecontrol.presentation.base.schedulers.SchedulersProvider
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable

class LineStopsRepository(private val faraDataSourceImpl: FaraDataSourceImpl,
                          private val schedulersProvider: SchedulersProvider) {

    val onLineStops: Flowable<LineStops> = faraDataSourceImpl.stops
            .subscribeOn(schedulersProvider.background())
            .observeOn(schedulersProvider.ui())
            .toFlowable(BackpressureStrategy.LATEST)
}