package com.krzysztofcywinski.fara.domain.datasource

import com.krzysztofcywinski.fara.domain.model.TimeTable
import io.reactivex.Observable


interface ZtmDataSource {

    fun getTimeTable(id: String, busstopId: Long, busstopNr: String, line: String, apikey: String): Observable<TimeTable>

}
